#!/bin/bash

# Submission script for GridEngine (GE). Each job will 
# be executed via the jobScript.sh
# This jobScript supports up to 7 parameters. Edit 
# the user specific part of the script according to 
# your program.
#
# Input to the script is a filelist with 1 file per line.
# For each file a job is started. With the parameter 
# nFilesPerJob a comma separated filelist will be 
# generated and handed to the job script. This feature
# is usefull when running many small jobs. Each
# job has its own logfile. All needed directories for the
# logfiles will be created if non existing.
#
# IMPORTANT: the hera/prometheus cluster jobs will only
# see the /hera file system. All needed scripts, programs
# and parameters have to be located on /hera or the job
# will crash. This script syncs your working dir to the submission
# dir on /hera . Make sure your scripts use the submission dir!
# Software should be taken from /cvmfs/hades.gsi.de/install/
#
# job log files will be named like inputfiles. If nFilesPerJob > 1
# the log files will contain the partnumber.
#
######################################################################
#   CONFIGURATION

################# PREDEFINITION OF IMPORTANT VALUES ##################
collidingNuclei=("oni" "nini" "auau")
collisionEnergy=("2gev" "1.93gev" "1.24gev")
function indexForCollisionArray {
   index=255 #negative values could not be returned by bash!!
   for (( ic=0; ic<${#collidingNuclei[@]}; ic++ ))
   do
      if [[ ${collidingNuclei[$ic]} == ${1,,} ]]
      then
         index=$ic
      fi
   done
   return $index
}

declare -a arrayMcbmSetups=()
function fillSupportedMcbmSetups {
   dirCbmroot="/u/lchlad/CBM/cbmroot/geometry/setup/"
   find ${dirCbmroot} -name "setup_mcbm_beam_*.C" > tmp_mcbmsetup.delme
   while read line; do
      bnLine=$(basename $line)
      setupName=${bnLine#*setup_}
      setupName=${setupName%.C*}
      arrayMcbmSetups+=("${setupName}")
   done < tmp_mcbmsetup.delme
   rm tmp_mcbmsetup.delme
}
fillSupportedMcbmSetups

function indexOfMcbmSetup {
   index=255 #negative values could not be returned by bash!!
   for (( ic=0; ic<${#arrayMcbmSetups[@]}; ic++ ))
   do
      if [[ ${arrayMcbmSetups[$ic]} == ${1,,} ]]
      then
         index=$ic
      fi
   done
   return $index
}
######################################################################


user=$(whoami)
currentdir=$(pwd | xargs -i basename {})                           
currentDir=$(pwd)

retval=0
export SLURMRETURN=0

collsystem=${1}   # options: oni , nini, auau (possibly others -> to be added!!)
mcbmsetup=${2}    # options: mcbm_beam_2021_07_surveyed, mcbm_beam_2022_02, mcbm_beam_2022_03, mcbm_beam_2022_04, mcbm_beam_2022_05 
macrotype=${3}    # expect "trans" or "digi" or "recoevt" or "hadronana"

indexForCollisionArray ${collsystem}
indCollSys=$?
if [[ $indCollSys -eq 255 ]]
then
   echo "Unsupported collision system!! You have requested: ${collsystem} but the options are"
   for str in ${collidingNuclei[@]}
   do
      echo ${str}
   done
   exit 1
fi

indexOfMcbmSetup ${mcbmsetup}
indMcbmSetup=$?
if [[ $indMcbmSetup -eq 255 ]]
then
   echo "Unsupported mCBM setup!! You have requested: ${mcbmsetup} but the options are"
   for str in ${arrayMcbmSetups[@]}
   do
      echo ${str}
   done
   exit 1
fi

numberOfParams=$#      #number of parameters to be used in sendScript
additionalparam=("$@") #this will put all parameters into array (including two necessary - mcbmsetup and macrotype)
additionalparam=("${additionalparam[@]:3}") #this will remove first three elemets (collision system, mcbmsetup and macrotype)
nAddParams=${#additionalparam[@]} #number of additional params after removal

function setNJobs {
   njobs=1000             #default value
   for (( iAP=0; iAP<$nAddParams; iAP++ ))
   do
      if [[ ${additionalparam[$iAP]} =~ "maxjobs" ]]
      then
         njobs=${additionalparam[$iAP]//[^0-9]/}
      fi
   done
   echo $njobs
}
nJobParts=$(setNJobs)
if [[ "${additionalparam[-1]}" == "debug" ]]
then
   nJobParts=1
   nAddParams=$((nAddParams-1))
fi

function setNEvents {
   nevts=100000             #default value
   for (( iAP=0; iAP<$nAddParams; iAP++ ))
   do
      if [[ ${additionalparam[$iAP]} =~ "maxevents" ]]
      then
         nevts=${additionalparam[$iAP]//[^0-9]/}
      fi
   done
   echo $nevts
}
nEvents=$(setNEvents)

submmissionbase=/lustre/cbm/users/${user}/sub
submissiondir=${submmissionbase}/${currentdir}
   outputbase=/lustre/cbm/users/${user}/mcbm_sim/${currentdir}/${collidingNuclei[$indCollSys]}_${collisionEnergy[$indCollSys]}/${mcbmsetup}   # outputdir for files AND logFiles
pathoutputlog=/lustre/cbm/users/${user}/mcbm_sim/logs     # protocol from batch farm for each file
jobscript=${submmissionbase}/${currentdir}/jobArrayScript.sh     # exec script (full path, call without dot, set it executable!)

par1=/lustre/cbm/users/lchlad/cbmroot/install_debian10/bin/CbmRootConfig.sh

if [[ "$macrotype" == "trans" ]]
then
#TRANSPORT MACRO
resources="--account=cbm --partition=long --time=1-00:00:00"
par2=${submissiondir}/mcbm_transport.C                               # optional par2 : executable
#par2=${submissiondir}/anna_mcbm_transport.C                               # optional par2 : executable
par3=${nEvents}                                                            # optional par3 : number of events 
par4=${mcbmsetup}                                    # optional par4 : specification of detectors setup 
par5=""                                                              # optional par5 : outputfile with path
par6=""                                                              # optional par6 : input file list (just 1 file per job!!)
par7=0                                                               # use Geant3 (default =0 is Geant4 in my version)
par8=""                                                              # nothing
par9=""

elif [[ "$macrotype" == "mixdigi" ]]
then
#EMBEDING MACRO ON DIGI LEVEL
resources="--account=cbm --time=0-08:00:00"
par2=${submissiondir}/mcbm_digi_mixed.C                               # optional par2 : executable
par3=""                                                       # optional par3 : input1 
par4=""                                                       # optional par4 : input2                                                    
par5=100000                                                   # optional par5 : event rate for input 1 [1/s]
par6=100000                                                   # optional par6 : event rate for input 2 [1/s]
par7=10000                                                    # optional par7 : length of time slice in [ns]
par8=""                                                       # optional par8 : outputname
par9=""

elif [[ "$macrotype" == "embdigi" ]]
then
#EMBEDING MACRO ON DIGI LEVEL
resources="--account=cbm --time=0-08:00:00"
par2=${submissiondir}/mcbm_digi_embed.C                               # optional par2 : executable
par3=""                                                       # optional par3 : input1 
par4=""                                                       # optional par4 : input2                                                    
par5=100000                                                   # optional par5 : event rate for input 1 [1/s]
par6=100000                                                   # optional par6 : event rate for input 2 [1/s]
par7=10000                                                    # optional par7 : length of time slice in [ns]
par8=1                                                        # optional par8 : event (1) or time (0) based
par9=""                                                       # optional par9 : outputname

elif [[ "$macrotype" == "digi" ]]
then
#DIGI MACRO
resources="--account=cbm --time=0-08:00:00"
par2=${submissiondir}/mcbm_digi.C                               # optional par2 : executable
par3=${nEvents}                                                       # optional par3 : number of events
par4=""                                                       # optional par4 : input base name                                                    
par5=100000                                                   # optional par5 : event rate for input 1 [1/s]
par6=10000                                                    # optional par6 : length of time slice in [ns]
par7=1                                                        # optional par7 : do event base (0 for time base) 
par8=""                                                              # nothing
par9=""

elif [[ "$macrotype" == "thermal" ]]
then
#KF thermal source
resources="--account=cbm --time=0-02:00:00"
par2=${submissiondir}/kf_thermal_signal_generator.C                               # optional par2 : executable
par3=1                                                        # optional par3 : particle ID in CbmKFPartEfficiencies.h (K0s=0, Lambda=1,...)
par4=${nEvents}                                                   # optional par4 : number of events 
par5=2                                                       # optional par5 : kinetic energy of beam in AGeV
par6=""                                                       # optional par6 : output file name
par7="" 
par8=""                                                              # nothing
par9=""

elif [[ "$macrotype" == "recoevt" ]]
then
#RECO MACRO FOR EVENT BASED DIGI
resources="--account=cbm --time=0-05:00:00"
par2=${submissiondir}/mcbm_reco_event.C                               # optional par2 : executable
#par2=${submissiondir}/anna_mcbm_reco_event.C                               # optional par2 : executable
par3=${nEvents}                                                            # optional par3 : number of events 
par4=""                                                              # optional par4 : specification of detectors setup
par5=${mcbmsetup}                                    # optional par5 : specification of detectors setup 
par6=0                                                               # Matching of Hits to MCpoints 
par7=""                                                              # nothing
par8=""                                                              # nothing
par9=""

elif [[ "$macrotype" == "recotime" ]]
then
#RECO MACRO FOR TIME BASED DIGI
resources="--account=cbm --time=0-05:00:00"
par2=${submissiondir}/mcbm_reco.C                               # optional par2 : executable
par3=${nEvents}                                                            # optional par3 : number of events 
par4=""                                                              # optional par4 : specification of detectors setup
par5="Real"                                    # optional par5 : specification of detectors setup 
par6=${mcbmsetup}                                                              # nothing
par7=""                                                              # nothing
par8=""                                                              # nothing
par9=""

elif [[ "$macrotype" == "hadronana" ]]
then
#HADRON ANALYSIS
resources="--account=cbm --time=0-03:00:00"
par2=${submissiondir}/mcbm_hadron_analysis.C                               # optional par
par3=${nEvents}                                                            # optio
par4=${mcbmsetup}                                                              # optiona
par5=""                                    # optional par5 : input directory
par6=""                                    # optional par6 : output directory                                             
par7=""                                                # cut parameters
par8=""
par9=""

elif [[ "$macrotype" == "anatree" ]]
then
#CONVERT TREE MACRO
resources="--account=cbm --time=0-08:00:00"
par2=${submissiondir}/run_analysis_tree_maker.C                               # optional par
par3=""                                                            # path + base name of data
par4=${mcbmsetup}                                                              # setup identification
par5=""                                    # unigen input file
par6=""                                                                                
par7=""                                                
par8=""
par9=""

else
echo "WRONG MACRO TYPE --> USE 'trans' OR 'digi' OR 'embed' or 'reco' !!"
exit 1
fi

reTestInteger='^[0-9]+$'
reTestFloat='^[0-9]+([.][0-9]+)?$'
reTestSignedFloat='^[+-]?[0-9]+([.][0-9]+)?$'

function indexOfInteger {
   index=255 #negative values could not be returned by bash!!
   for iAP in "${!additionalparam[@]}"
   do
      if [[ ${additionalparam[$iAP]} =~ $reTestInteger ]]
      then
         index=$iAP
      fi
   done
   return $index
}

function setTopoCuts {
   tcs=2             #default value TopoCutSet
   for (( iAP=0; iAP<$nAddParams; iAP++ ))
   do
      if [[ ${additionalparam[$iAP],,} =~ "topocut" ]]
      then
         tcs=${additionalparam[$iAP]//[^0-9]/}
      fi
   done
   echo $tcs
}

function wantedGeant3 {
   foundG3=0
   for str in "${additionalparam[@]}"
   do
      if [[ "${str,,}" == "geant3" ]]
      then
         foundG3=1
      fi
   done
   return $foundG3
}

function time2seconds {
    timeInSec=-1
    nColon=$(echo $1 | grep -o ":" | wc -l)
    nDash=$(echo $1 | grep -o "-" | wc -l)
    if [[ $nDash -eq 1 ]]
    then
        days=$(echo $1 | cut -d "-" -f1)
        dayTime=$(echo $1 | cut -d "-" -f2)
        hours=$(echo $dayTime | cut -d ":" -f1)
        minutes=$(echo $dayTime | cut -d ":" -f2)
        seconds=$(echo $dayTime | cut -d ":" -f3)
        days=$((10#${days}))
        hours=$((10#${hours}))
        minutes=$((10#${minutes}))
        seconds=$((10#${seconds}))
        timeInSec=$(( days*86400 + hours*3600 + minutes*60 + seconds ))
        echo $timeInSec
    else
        if [[ $nColon -eq 1 ]]
        then
            minutes=$(echo $1 | cut -d ":" -f1)
            seconds=$(echo $1 | cut -d ":" -f2)
            minutes=$((10#${minutes}))
            seconds=$((10#${seconds}))
            timeInSec=$(( minutes*60 + seconds ))
            echo $timeInSec
        elif [[ $nColon -eq 2 ]]
        then
            hours=$(echo $1 | cut -d ":" -f1)
            minutes=$(echo $1 | cut -d ":" -f2)
            seconds=$(echo $1 | cut -d ":" -f3)
            hours=$((10#${hours}))
            minutes=$((10#${minutes}))
            seconds=$((10#${seconds}))
            timeInSec=$(( hours*3600 + minutes*60 + seconds ))
            echo $timeInSec
        else
            echo $timeInSec
        fi
    fi
}

function setMaxTime {
   for (( iAP=0; iAP<$nAddParams; iAP++ ))
   do
      if [[ ${additionalparam[$iAP]} =~ "maxtime" ]]
      then
         maxTime=${additionalparam[$iAP]:8}          # removing pattern "maxtime=" from the string
         requestedTimeInSec=$(time2seconds $maxTime) # return time in seconds for the format (D-)(HH:)MM:SS
         ((mainPartitionLimit=8*3600))               # max time for main partition is 8h
         longAlready=$(grep -i -E long $resources | wc -l)   # is long partition requested in resources
         if [[ $requestedTimeInSec -gt $mainPartitionLimit && $longAlready -eq 0 ]]; then resources=$(echo "$resources --partition=long"); fi
         resources=$(echo "$resources" | sed "s/\(time=\).\{10\}/\1${maxTime}/")
      fi
   done
}

function printDigiSettings {
   switcher=255
   index=255
   for (( iAP=0; iAP<$nAddParams; iAP++ ))
   do
      if [[ ${additionalparam[$iAP],,} == "event" ]]
      then
         switcher=1
      elif [[ ${additionalparam[$iAP],,} == "time" ]]
      then
         switcher=0
         index=$iAP
      fi
   done
   
   if [[ ${switcher} -eq 255 ]]
   then
      # nor 'event' either 'time' found in the parameters !! -> using default event base...
      echo "1 1 1"
   elif [[ ${switcher} -eq 1 ]]
   then
      echo "1 1 1" #only first number is important!!
   elif [[ ${switcher} -eq 0 ]]
   then
      index=$((index+1))
      evtRate=${additionalparam[$index]}
      index=$((index+1))
      tsLength=${additionalparam[$index]}
      echo "0 ${evtRate} ${tsLength}"
   fi   
}

function wantedMcMatching {
   foundMC=0
   for str in "${additionalparam[@]}"
   do
      if [[ "${str,,}" == "matchmc" ]]
      then
         foundMC=1
      fi
   done
   return $foundMC
}


  supportedDataset=("lambda"              "urqmd"         "lukasurqmd"   "mixbeam"    "emblambda"    "beam")
   filenameDataset=("lambda_thermal_2GeV" "urqmd"         "urqmd"        "urqmd_beam" "urqmd_lambda" "beam")
    dirpathDataset=("${outputbase}"       "${outputbase}" "/lustre/cbm/users/lchlad/mcbm_sim/lambda_mcbm/${collidingNuclei[$indCollSys]}_${collisionEnergy[$indCollSys]}/${mcbmsetup}" "${outputbase}" "${outputbase}" "${outputbase}")

function indexOfDataset {
   index=255 #negative values could not be returned by bash!!

   for str in "${additionalparam[@]}"
   do
      for (( ic=0; ic<${#supportedDataset[@]}; ic++ ))
      do
         if [[ ${supportedDataset[$ic]} == ${str,,} ]]
         then
            index=$ic
         fi
      done
   done

   return $index
}

indexOfDataset
iDS=$?
if [[ $iDS -eq 255 ]]
then
   echo "Not recognized DATASET!! Specify within ${additionalparam[*]} one of:"
   for str in ${supportedDataset[@]}
   do
      echo ${str}
   done
   exit 1
fi
dsFileName=${filenameDataset[$iDS]}
dsInputBase=${dirpathDataset[$iDS]}

wantedGeant3
if [ $? -eq 1 ]
then
   dsFileName=${dsFileName}_geant3
fi

setMaxTime

jobarrayFile="JAfiles/jobarray_${mcbmsetup}_${macrotype}" 
for (( iAP=0; iAP<$nAddParams; iAP++ ))
do 
   jobarrayFile="${jobarrayFile}_${additionalparam[$iAP]}"
done
jobarrayFile="${jobarrayFile}.dat"

######################################################################

#---------------------------------------------------------------------
# create sub dirs
if [ ! -d $submmissionbase ]
then
    echo "===> CREATE SUBMISSIONBASEDIR : $submmissionbase"
    mkdir -p $submmissionbase
else
    echo "===> USE SUBMISSIONBASEDIR : $submmissionbase"
fi

#---------------------------------------------------------------------
# output dirs

if [ ! -d $outputbase ]
then
   echo "===> CREATE OUTPUTBASE : $outputbase"
   mkdir -p $outputbase
else
   echo "===> USE OUTPUTBASE : $outputbase"
fi

if [ ! -d $pathoutputlog ]
then
   echo "===> CREATE LOGDIR : $pathoutputlog"
   mkdir -p $pathoutputlog
else
   echo "===> USE LOGDIR : $pathoutputlog"
fi
#---------------------------------------------------------------------
# read the files list into an job array
if [ -f $jobarrayFile ]
then
  rm -f $jobarrayFile
fi

echo "===> CREATING JOB ARRAY FILE"
ctF=0          # counter for file number
while ((ctF<$nJobParts))
do
   ######################################################################
   #  SEND NEW JOB (USER SPECIFIC)
   outputdir=${outputbase}/${ctF}
   dsInputDir=${dsInputBase}/${ctF}
   if [ ! -d $outputdir ]
   then
      mkdir -p $outputdir
   fi 
   
   #===============================================================
   if [[ "$macrotype" == "trans" ]]
   then
      par5=${outputdir}/${dsFileName}

      if [ $iDS -eq 0 ] # THERMAL LAMBDA
      then
         par6=${dsInputDir}/${dsFileName}.kfsig.root
      elif [ $iDS -eq 1 ] # OFFICIAL URQMD
      then
         shiftedvalue=$((ctF+1))
         par6=/lustre/cbm/prod/gen/urqmd/${collidingNuclei[$indCollSys]}/${collisionEnergy[$indCollSys]}/mbias/urqmd.${collidingNuclei[$indCollSys]}.${collisionEnergy[$indCollSys]}.mbias.$(printf "%05d" $shiftedvalue).root
      else
         echo "For TRANSPORT only: 'lambda' and 'urqmd' are supported datasets!!"
         exit 1
      fi

      wantedGeant3
      if [ $? -eq 1 ]
      then
         par7=1
      fi

   #===============================================================
   elif [[ "$macrotype" == "mixdigi" ]]
   then
      par3=${dsInputDir}/${filenameDataset[1]} #UrQMD
      par4=${dsInputDir}/${filenameDataset[5]} #Beam transport
      par8=${outputdir}/${filenameDataset[3]}  #Mixed
      
      wantedGeant3
      if [ $? -eq 1 ]
      then
         par3=${par3}_geant3
         par4=${par4}_geant3
         par8=${par8}_geant3
      fi
      #time settings a bit complicated due to 2 sources!! TODO


   #===============================================================
   elif [[ "$macrotype" == "embdigi" ]]
   then
      par3=${dsInputDir}/${filenameDataset[1]} #UrQMD
      par4=${dsInputDir}/${filenameDataset[0]} #Lambda
      par9=${outputdir}/${filenameDataset[4]}  #Embedded

      wantedGeant3
      if [ $? -eq 1 ]
      then
         par3=${par3}_geant3
         par4=${par4}_geant3
         par8=${par8}_geant3
      fi
      #time settings a bit complicated due to 2 sources!! TODO
      par8=1 #event based now!!

   #===============================================================
   elif [[ "$macrotype" == "digi" ]]
   then
      myDigiSet=$(printDigiSettings)
      par7=$(echo ${myDigiSet} | cut -d " " -f1)
      par5=$(echo ${myDigiSet} | cut -d " " -f2)
      par6=$(echo ${myDigiSet} | cut -d " " -f3)

      par4=${outputdir}/${dsFileName}

   #===============================================================
   elif [[ "$macrotype" == "recoevt" ]]
   then
      par4=${outputdir}/${dsFileName}

      wantedMcMatching
      if [ $? -eq 1 ]
      then
         par6=1
      fi

   #===============================================================
   elif [[ "$macrotype" == "recotime" ]]
   then
      par4=${outputdir}/${dsFileName}


   #===============================================================
   elif [[ "$macrotype" == "thermal" ]]
   then
      par6=${outputdir}/${dsFileName}.kfsig.root

   #===============================================================
   elif [[ "$macrotype" == "hadronana" ]]
   then
      par5=${dsInputDir}/${dsFileName}
      par6=${outputdir}/${dsFileName}
      par7=$(setTopoCuts)

   fi #end of options for macrotype

   echo "${par1} ${par2} ${par3} ${par4} ${par5} ${par6} ${par7} ${par8} ${par9}" >>  $jobarrayFile

   ######################################################################
   ((ctF+=1)) 
done
#---------------------------------------------------------------------

#---------------------------------------------------------------------
# sync the local modified stuff 
# to the submission dir
echo "===> SYNC CURENTDIR TO SUBMISSIONDIR : rsync  -vHa $currentDir ${submmissionbase}"
rsync -vHa --exclude={".git","merged_root_files","testing","nohup_outputs"} $currentDir ${submmissionbase}/

syncStat=$?

if [ ! $syncStat -eq 0 ]
then
     echo "===> ERROR : SYNCHRONIZATION ENCOUNTERED PROBLEMS"
else

  echo "-------------------------------------------------"

  nFiles=$( cat $jobarrayFile | wc -l)
  ctsend=0
  block=1000
  while ((${ctsend} * ${block} < ${nFiles}))
  do
     if [ $retval -eq 0 ]
     then
	     ((start=${ctsend}*${block}+1))
        ((stop= ${start}+${block}-1))
        ((rest=${nFiles}-${start}))
        if [ $rest -le $block ]
        then
           ((stop=$start+$rest))
        fi
                                                                           #last output from sendscript should be thanks to this the jobID
        command="--array=${start}-${stop} --begin=now+30 ${resources} -D ${submissiondir} --parsable  --output=${pathoutputlog}/slurm-%A_%a.out -- ${jobscript} ${submissiondir}/${jobarrayFile} ${pathoutputlog} ${macrotype}"
        echo $command
        sbatch $command
        retval=$?  
        if [ $retval -ne 0 ]
        then
           echo "sbatch returned $retval"
           export SLURMRETURN=1
        fi
     else
       echo "ERROR SUBMITTING TO SLURM: FILES NOT SEND offset ${arrayoffset} to ${stop}!"
     fi

     ((ctsend+=1))
  done

fi
