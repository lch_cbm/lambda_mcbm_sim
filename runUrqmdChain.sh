#!/bin/bash

debugging=0
verboseMode=0
keepLogMode=0
dirWithSendscript="/u/lchlad/CBM/lambda_mcbm"
path2logfiles="/lustre/cbm/users/lchlad/mcbm_sim/logs"

declare -a problematicalyFinishedTasks=()

###########################################
# USEFUL FUNCTION FOR CHECKING JOB STATUS #
###########################################
function time2seconds {
    timeInSec=-1
    nColon=$(echo $1 | grep -o ":" | wc -l)
    nDash=$(echo $1 | grep -o "-" | wc -l)
    if [[ $nDash -eq 1 ]]
    then
        days=$(echo $1 | cut -d "-" -f1)
        dayTime=$(echo $1 | cut -d "-" -f2)
        hours=$(echo $dayTime | cut -d ":" -f1)
        minutes=$(echo $dayTime | cut -d ":" -f2)
        seconds=$(echo $dayTime | cut -d ":" -f3)
        days=$((10#${days}))
        hours=$((10#${hours}))
        minutes=$((10#${minutes}))
        seconds=$((10#${seconds}))
        timeInSec=$(( days*86400 + hours*3600 + minutes*60 + seconds ))
        echo $timeInSec
    else
        if [[ $nColon -eq 1 ]]
        then
            minutes=$(echo $1 | cut -d ":" -f1)
            seconds=$(echo $1 | cut -d ":" -f2)
            minutes=$((10#${minutes}))
            seconds=$((10#${seconds}))
            timeInSec=$(( minutes*60 + seconds ))
            echo $timeInSec
        elif [[ $nColon -eq 2 ]]
        then
            hours=$(echo $1 | cut -d ":" -f1)
            minutes=$(echo $1 | cut -d ":" -f2)
            seconds=$(echo $1 | cut -d ":" -f3)
            hours=$((10#${hours}))
            minutes=$((10#${minutes}))
            seconds=$((10#${seconds}))
            timeInSec=$(( hours*3600 + minutes*60 + seconds ))
            echo $timeInSec
        else
            echo $timeInSec
        fi
    fi
}

TIMERATIOTOKILL="0.33" #running time / time-limit , recommanded 0.33
function checkRunningTime {
    if [[ $debugging -eq 1 ]]; then squeue --array -j ${1} --state=RUNNING -o "%M %l"; fi
    squeue --noheader --array -j ${1} --state=RUNNING -o "%M %l"  > tmp_${1}.delme

    sumRunningTime=0
    sumTimeLimit=0
    n=0

    while read line; do
        strRunningTime=$(echo $line | cut -d " " -f1)
        strTimeLimit=$(echo $line | cut -d " " -f2)

        runTime=$(time2seconds $strRunningTime)
        limitTime=$(time2seconds $strTimeLimit)

        sumRunningTime=$(( sumRunningTime + runTime ))
        sumTimeLimit=$(( sumTimeLimit + limitTime ))
        n=$((n+1))
    done < tmp_${1}.delme

    rm tmp_${1}.delme
    
    if [[ $n -gt 0 ]]
    then
        awk -v sumRT="$sumRunningTime" -v sumLT="$sumTimeLimit" -v d="$debugging" -v cr="$TIMERATIOTOKILL" 'BEGIN {ratio=sumRT/sumLT; if(d==1) {printf "%d\t%d\t%4.3f\n", sumRT, sumLT, ratio}; if(ratio>cr) {print 1} else {print 0}}'
    else
        echo 0 # probably no running jobs -> is checked with collective condition
    fi
}

STIME=60 #sleep time in seconds used in while loop
ALLOWEDKILLRATIO=1 #in percent , recommanded 1
function checkJobStatus {
  NumberOfJobsInArray=$(squeue --noheader --array -j ${1} | wc -l)
  echo "         number of submitted jobs: ${NumberOfJobsInArray}"
  sleep 40 # jobs submitted with 30s waiting time for resources allocation to make sure that number of submitted jobs will be correct
  LimitForUnfinished=$(( ALLOWEDKILLRATIO * NumberOfJobsInArray / 100))
  StillPendingJobs=$(squeue --noheader --array -j ${1} --state=PENDING | wc -l)
  NotFinishedJobs=$(squeue --noheader --array -j ${1} --state=RUNNING | wc -l)
  JobsRunningForToLong=$(checkRunningTime ${1} | tail -1)
  if [[ $debugging -eq 1 ]]
  then 
    echo "If there will be less then $LimitForUnfinished jobs running for more than $TIMERATIOTOKILL of time-limit, they will be cancelled."
    checkRunningTime ${1}; printf "%d\t%d\t%d\n" "$StillPendingJobs" "$NotFinishedJobs" "$JobsRunningForToLong"
  fi

  NotStartedJobs=$StillPendingJobs
  zeroPendingCounter=0

  while [[ $StillPendingJobs -gt 0 || $NotFinishedJobs -gt $LimitForUnfinished || ($NotFinishedJobs -ge 1 && $JobsRunningForToLong -eq 0) ]]
  do
    sleep $STIME
    StillPendingJobs=$(squeue --noheader --array -j ${1} --state=PENDING | wc -l)
    NotFinishedJobs=$(squeue --noheader --array -j ${1} --state=RUNNING | wc -l)
    JobsRunningForToLong=$(checkRunningTime ${1} | tail -1)
    if [[ $debugging -eq 1 ]]; then checkRunningTime ${1}; printf "%d\t%d\t%d\n" "$StillPendingJobs" "$NotFinishedJobs" "$JobsRunningForToLong"; fi

    if [[ $StillPendingJobs -eq 0 ]]
    then
        zeroPendingCounter=$((zeroPendingCounter+1))
        if [[ $zeroPendingCounter -gt 1 ]]; then NotStartedJobs=$StillPendingJobs; fi
    else
        zeroPendingCounter=0
        NotStartedJobs=$StillPendingJobs
    fi
  done

  scancel ${1}
  sleep 10
  echo "         number of jobs/tasks from the jobArray with corresponding final state: "
  printf "%7d%26s\t%s\n" "${NotStartedJobs}" "DID NOT START" "(tasks had to be cancelled externally)"
  printf "%7d%26s\t%s\n" "${NotFinishedJobs}"  "CANCELLED to save time" "(this is included in: CANCELLED by $(id -u $(whoami)))"
}

function addProblematicTaskId {
    if [[ ${1} != *"["* ]]  #string does not contain square brackets
    then
        problematicalyFinishedTasks+=("${1}")
    else
        insideBrackets=$(echo "${1}" | awk -F '[][]' '{print $2}')
        nIntervals=$(echo ${insideBrackets} | grep -o "," | wc -l)
        for ((iInt=0; iInt<=${nIntervals}; iInt++)) 
        do
            posInterval=$((iInt+1))
            strInterval=$(echo ${insideBrackets} | cut -d ',' -f ${posInterval})
            intStart=$(echo ${strInterval} | cut -d '-' -f1)
            intEnd=$(echo ${strInterval} | cut -d '-' -f2)
            for ((jTask=${intStart}; jTask<=${intEnd}; jTask++))
            do
                problematicalyFinishedTasks+=("${jTask}")
            done
        done
    fi
}

ALLOWEDNOTCOMPLETEDRATIO=10 #in percentage from number of executed jobs
finalizeJobTasks() {  #because of using 'local' variable inside one can not use declaration like 'function foo {...}' but rather 'foo() {...}'
    local OPTIND OPTION locVerbMode resubMode
    locVerbMode=${verboseMode}
    resubMode=0
    while getopts ':rv' OPTION; do
        case "${OPTION}" in
            r)
                resubMode=1
                ;;
            v)
                locVerbMode=1
                ;;
            ?)
                echo "Usage: finalizeJobTasks [-r(esubmision)] [-v(erbose)] <JobID>"
                exit 1
                ;;
        esac
    done
    shift $((OPTIND-1))

    if [[ $locVerbMode -eq 1 ]]
    then
        sacct -j ${1} -X --noheader --format=state%25 | sort | uniq -c  # -X option effectively ignore the steps (showing only the status for whole job)
        nSuccessfullyFinished=$(grep Macro ${path2logfiles}/slurm-${1}_* | grep finished | grep successfully | wc -l)
        printf "%7d%26s\t%s\n" "${nSuccessfullyFinished}" "SUCCESSFULLY FINISHED" "(based on checks of job's log files)"
    fi

    declare ListNotCompletedJobs=($(sacct -j ${1} -X --format=state,jobid%100 --noheader | grep -i -v -E completed | awk '{print $2}'))
    nEligibleJobs=$(sacct -j ${1} -X --format=jobid --noheader | wc -l)
    nNotCompletedJobs=${#ListNotCompletedJobs[@]}
    LimitForNotCompleted=$(( ALLOWEDNOTCOMPLETEDRATIO * nEligibleJobs / 100))
    if [[ $nNotCompletedJobs -lt $LimitForNotCompleted ]]
    then
        # reasonable number of jobs finished successfully -> note the bad jobs and remove logs
        for badjobid in ${ListNotCompletedJobs[@]}
        do
            mv ${path2logfiles}/slurm-${badjobid}.log ${path2logfiles}/slurm-${badjobid}.checkme 2>/dev/null #silent crashes if file does not exist
            mv ${path2logfiles}/slurm-${badjobid}.out ${path2logfiles}/slurm-${badjobid}.checkme 2>/dev/null #silent crashes if file does not exist
    
            jobidLength=$(expr length ${1})      # length of jobId string
            jobidLength=$((jobidLength+1))       # add 1 for the underscore
            taskId=${badjobid:${jobidLength}}    # string after the underscore - can be [with ranges] or taskId number
            addProblematicTaskId $taskId
        done

        if [[ $debugging -eq 0 && $keepLogMode -eq 0 ]]
        then
            rm -f ${path2logfiles}/slurm-${1}_*.log
        fi

        return 0
    else
        # a lot of jobs crashed for some reason -> resubmision will be envoke, do not note the bad jobs unless already resubmitted, remove all logs if not resubmitted
        if [[ $resubMode -eq 0 ]]
        then
            if [[ $debugging -eq 0 && $keepLogMode -eq 0 ]]
            then
                rm -f ${path2logfiles}/slurm-${1}_*
            fi
        else
            for badjobid in ${ListNotCompletedJobs[@]}
            do
                mv ${path2logfiles}/slurm-${badjobid}.log ${path2logfiles}/slurm-${badjobid}.checkme 2>/dev/null #silent crashes if file does not exist
                mv ${path2logfiles}/slurm-${badjobid}.out ${path2logfiles}/slurm-${badjobid}.checkme 2>/dev/null #silent crashes if file does not exist
    
                jobidLength=$(expr length ${1})      # length of jobId string
                jobidLength=$((jobidLength+1))       # add 1 for the underscore
                taskId=${badjobid:${jobidLength}}    # string after the underscore - can be [with ranges] or taskId number
                addProblematicTaskId $taskId
            done

            if [[ $debugging -eq 0 && $keepLogMode -eq 0 ]]
            then
                rm -f ${path2logfiles}/slurm-${1}_*.log
            fi
        fi

        return 1
    fi
}

removePreviouslyNotcompletedTasks() {
    local OPTIND OPTION locVerbMode
    locVerbMode=${verboseMode}
    while getopts ':v' OPTION; do
        case "${OPTION}" in
            v)
                locVerbMode=1
                ;;
            ?)
                echo "Usage: removePreviouslyNotcompletedTasks [-v(erbose)] <JobID>"
                exit 1
                ;;
        esac
    done
    shift $((OPTIND-1))

    for badTask in ${problematicalyFinishedTasks[@]}
    do
        taskToCancel="${1}_${badTask}"
        if [[ $locVerbMode -eq 1 ]]; then echo "         canceling job ${taskToCancel}  <-- there was issue in one of previous steps"; fi
        scancel ${taskToCancel}
    done
}

function preSubmitionTasks {
    cd $dirWithSendscript
    printf "\n"
    echo "__________$(date)__________"
}

function seconds2slurmtime {
    nsecs=${1}
    days=$(( nsecs /86400 ))
    hours=$(( (nsecs -days*86400) /3600 ))
    minutes=$(( (nsecs -days*86400 -hours*3600) /60 ))
    seconds=$(( nsecs -days*86400 -hours*3600 -minutes*60 ))
    printf "%1d-%02d:%02d:%02d\n" "$days" "$hours" "$minutes" "$seconds" 
}

reSubmitSimStep() {
    previousJobArrayID=$1
    resubMacroName=$2
    jobParams=("$@")
    jobParams=("${jobParams[@]:2}")

    #check if the reason for crashing was Timelimit if yes increase time limit (partition will be checked in sendScript)
    nEligibleJobs=$(sacct -j ${previousJobArrayID} -X --format=jobid --noheader | wc -l)
    nTimeoutJobs=$(sacct -j ${previousJobArrayID} -X --format=state --noheader | grep -i -E timeout | wc -l)
    LimitForTimeout=$(( ALLOWEDNOTCOMPLETEDRATIO * nEligibleJobs / 100))
    if [[ $nTimeoutJobs -gt $LimitForTimeout ]]
    then
        previousTimelimit=$(sacct -j ${previousJobArrayID} -X --format=TimelimitRaw --noheader | head -1)
        previousTimelimit=$((10#${previousTimelimit}))
        previousTimelimit=$((previousTimelimit *60))    # time in seconds
        newTimelimit=$((previousTimelimit *2))          # new time limit will be double the previous (should be enough...)
        newTimelimit=$(seconds2slurmtime $newTimelimit) # convert to SLURM string format for timelimit
        newTimelimit=$(echo "maxtime=$newTimelimit")

        #replace the existing 'maxtime' value with this one, if not used before add it (ntb: ${!array[@]} where '!' returns index of element)
        ctMT=0
        for ip in "${!jobParams[@]}"
        do
            if [[ ${jobParams[$ip],,} =~ "maxtime" ]]
            then
                ${jobParams[$ip]}=$newTimelimit
                ctMT=$((ctMT+1))
                break
            fi
        done
        if [[ $ctMT -eq 0 ]]; then jobParams+=("$newTimelimit"); fi
    fi

    #do the re-submition
    preSubmitionTasks
    echo "Sending jobArray with params: ${jobParams[*]}"
    RESUBJOB_ID=$(bash sendJobArrayScript.sh  ${jobParams[*]} | tail -1)
    if [ -n "$RESUBJOB_ID" ] && [ "$RESUBJOB_ID" -eq "$RESUBJOB_ID" ] 2>/dev/null; then
        echo "---> ${resubMacroName} macro REsubmitted with jobID: ${RESUBJOB_ID}"
        removePreviouslyNotcompletedTasks $RESUBJOB_ID
        checkJobStatus $RESUBJOB_ID
        finalizeJobTasks -r -v $RESUBJOB_ID
    else
        echo "---> ERROR: Something went wrong when submitting: sendJobArrayScript.sh ${jobParams[*]}"
        echo "---> ERROR: RESUBJOB_ID=${RESUBJOB_ID}"
    fi
}


###########################
# MAIN PART OF THE SCRIPT #
###########################
echo "This script is/was running on machine:  $(uname -n)  with task pid:  $$"
echo "You are $(whoami) with UID = $(id -u $(whoami))"

# main script options
userMaxEvents=""
userMaxTime=""
userMaxJobs=""
while getopts ':de:j:kt:v' OPTION; do
    case "${OPTION}" in
        d)
            debugging=1
            ;;
        e)
            userMaxEvents=${OPTARG}
            ;;
        j)
            userMaxJobs=${OPTARG}
            ;;
        k)
            keepLogMode=1
            ;;
        t)
            nColon=$(echo ${OPTARG} | grep -o ":" | wc -l)
            nDash=$(echo ${OPTARG} | grep -o "-" | wc -l)
            if [[ nDash -eq 1 && nColon -eq 2 ]]
            then
                userMaxTime=${OPTARG}
            else
                echo "Keep the time format as: -t D-HH:MM:SS"
                exit 1
            fi
            ;;
        v)
            verboseMode=1
            ;;
        ?)
            echo "Usage: $(basename $0) [-d(ebugging)] [-e <MaxEvents>] [-j <MaxJobs>] [-k(eep logs)] [-t <MaxTime(D-HH:MM:SS)>] [-v(erbose)] <CollSys> <McbmSetup> <AdditionalParams>"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

#NECESSARY PARAMETERS
coll_system=${1} # which collision system one wants to run: oni = O+Ni @2A GeV, nini = Ni+Ni @1.93A GeV, auau = Au+Au @1.24A GeV
mcbm_setup=${2}  # different versions of mcbm_beam_YEAR_VV
dataset="urqmd"  # since this script is named runUrqmdChain => only urqmd is here used!!

#MORE PARAMETERS IF YOU WANT THEM, e.g. using geant3 instead of default geant4, maximum number of events or time for jobs (this is mainly for testing purposes)
specificParams=("$@")                     #this will put all parameters into array (including two necessary - collision system and mcbmsetup)
specificParams=("${specificParams[@]:2}") #this will remove first two elemets (mcbmsetup and macrotype)

#if you want to have a different number of events/jobs or maximum time for job (for testing purposes for example)
if [ ! -z ${userMaxEvents:+bla} ]; then specificParams+=("maxevents=${userMaxEvents}"); fi
if [ ! -z ${userMaxTime:+bla} ]; then specificParams+=("maxtime=${userMaxTime}"); fi
if [ ! -z ${userMaxJobs:+bla} ]; then specificParams+=("maxjobs=${userMaxJobs}"); fi
if [[ $debugging -eq 1 ]]; then specificParams+=("debug"); fi

individualSimSteps=("trans" "digi event" "recoevt matchMC" "hadronana topocut=3")
fullMacroName=("TRANSPORT" "DIGITIZATION" "RECONSTRUCTION" "HADRON ANALYSIS")

for (( i=0; i<${#individualSimSteps[@]}; i++ ))
do
    preSubmitionTasks
    mysjobParams="${coll_system} ${mcbm_setup} ${individualSimSteps[$i]} ${dataset} ${specificParams[*]}"
    echo "Sending jobArray with params: ${mysjobParams}"
    MYSJOB_ID=$(bash sendJobArrayScript.sh  ${mysjobParams} | tail -1)
    if [ -n "$MYSJOB_ID" ] && [ "$MYSJOB_ID" -eq "$MYSJOB_ID" ] 2>/dev/null; then
        echo "---> ${fullMacroName[$i]} macro submitted with jobID: ${MYSJOB_ID}"
        removePreviouslyNotcompletedTasks $MYSJOB_ID
        checkJobStatus $MYSJOB_ID
        finalizeJobTasks -v $MYSJOB_ID
        if [[ $? -ne 0 ]]; then reSubmitSimStep $MYSJOB_ID ${fullMacroName[$i]} ${mysjobParams}; fi
    else
        echo "---> ERROR: Something went wrong when submitting: sendJobArrayScript.sh ${mysjobParams}"
        echo "---> ERROR: MYSJOB_ID=${MYSJOB_ID}"
    fi
done


printf "\n"
echo "Do not forget to check and delete files: ${path2logfiles}/slurm-*.checkme !!"
echo "...everything is finished!!"
exit 0
