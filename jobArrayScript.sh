#!/bin/bash

# This is the generic jobscript to run jobs on GridEngine
#
# The script supports up to 7 parameters.
#
# The user specific part of the script is indicated below.



par1="no"
par2="no"
par3="no"
par4="no"
par5="no"
par6="no"
par7="no"
par8="no"

if [ $# -lt 1 ]
then
        echo "Usage : jobScript.sh par1 [par2] [par2] [par3] [par4] [par5] [par6] [par7]"
        sleep 3
        exit
fi

case "$#" in

1)  par1=$1
    ;;
2)  par1=$1
    par2=$2
    ;;
3)  par1=$1
    par2=$2
    par3=$3
    ;;
4)  par1=$1
    par2=$2
    par3=$3
    par4=$4
    ;;
5)  par1=$1
    par2=$2
    par3=$3
    par4=$4
    par5=$5
    ;;
6)  par1=$1
    par2=$2
    par3=$3
    par4=$4
    par5=$5
    par6=$6
    ;;
7)  par1=$1
    par2=$2
    par3=$3
    par4=$4
    par5=$5
    par6=$6
    par7=$7
    ;;
*) echo "Unsupported number of arguments" 
   echo "Usage : jobScript.sh par1 [par2] [par2] [par3] [par4] [par5] [par6] [par7]"
   exit
   ;;
esac

    echo ""               
    echo "--------------------------------"
    echo "SLURM_JOBID        : " $SLURM_JOBID
    echo "SLURM_ARRAY_JOB_ID : " $SLURM_ARRAY_JOB_ID
    echo "SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID
    echo "--------------------------------"
    echo ""
    
    macrotype=$par3
    pathoutputlog=$par2 
    jobarrayFile=$par1

    # map back params for the job
    input=$(awk "NR==$SLURM_ARRAY_TASK_ID" $jobarrayFile)   # get all params for this job
    
    
    par1=$(echo $input | cut -d " " -f1)
    par2=$(echo $input | cut -d " " -f2)
    par3=$(echo $input | cut -d " " -f3)
    par4=$(echo $input | cut -d " " -f4)
    par5=$(echo $input | cut -d " " -f5)
    par6=$(echo $input | cut -d " " -f6)
    par7=$(echo $input | cut -d " " -f7)
    par8=$(echo $input | cut -d " " -f8)
    par9=$(echo $input | cut -d " " -f9)

    echo "input: $input"
    echo "par1 = ${par1}"
    echo "par2 = ${par2}"
    echo "par3 = ${par3}"
    echo "par4 = ${par4}"
    echo "par5 = ${par5}"
    echo "par6 = ${par6}"
    echo "par7 = ${par7}"
    echo "par8 = ${par8}"
    echo "par9 = ${par9}"


    format='+%Y/%m/%d-%H:%M:%S'

    host=$(hostname)

    date $format
    echo ""               
    echo "--------------------------------"
    echo "RUNNING ON HOST : " $host
    echo "WORKING DIR     : " $(pwd)
    echo "USER is         : " $USER
    echo "DISK USAGE /tmp :"
    df -h /tmp
    echo "--------------------------------"
    
    
    echo ""
    echo "--------------------------------"
    echo " DEBUG INFO"                     
    echo "==> Kernel version information :"
    uname -a                               
    echo "==> C compiler that built the kernel:"
    cat /proc/version                           
    echo "==> load on this node:"               
    mpstat -P ALL                               
    echo "==> actual compiler is"               
    gcc -v                                      
    echo "--------------------------------"     
    echo ""               


###################################################################
###################################################################
#   EDIT THIS PART TO EXECUTE YOUR JOB!

workDir=$(pwd)
tempDir=$(mktemp -d)

echo "Working directory: ${workDir}"
echo "Temp work dir: ${tempDir}"

cd ${tempDir}
cp -v $par2 .
par2=$(basename $par2)

#----------------------
# evironment 
  echo "==> running enironment script ${par1}"
  source ${par1}
#----------------------

  echo "==> execute program "

  if [[ "$macrotype" == "trans" ]]
  then
  #TRANSPORT MACRO
  echo "==> root -l -b -q  $par2($par3,$par4,$par5,$par6,$par7)"
  time  root -l -b -q ''$par2'('$par3',"'$par4'","'$par5'","'$par6'",'$par7')'

  elif [[ "$macrotype" == "embdigi" ]]
  then
  #EMBEDING MACRO ON DIGI LEVEL
  echo "==> root -l -b -q  $par2($par3,$par4,$par5,$par6,$par7,$par8,$par9)"
  time  root -l -b -q ''$par2'("'$par3'","'$par4'",'$par5','$par6','$par7','$par8',"'$par9'")'

  elif [[ "$macrotype" == "mixdigi" ]]
  then
  #MIXING MACRO ON DIGI LEVEL
  echo "==> root -l -b -q  $par2($par3,$par4,$par5,$par6,$par7,$par8)"
  time  root -l -b -q ''$par2'("'$par3'","'$par4'",'$par5','$par6','$par7',"'$par8'")'


  elif [[ "$macrotype" == "digi" ]]
  then
  #DIGI MACRO
  echo "==> root -l -b -q  $par2($par3,$par4,$par5,$par6,$par7)"
  time  root -l -b -q ''$par2'('$par3',"'$par4'",'$par5','$par6','$par7')'

  elif [[ "$macrotype" == "recoevt" ]]
  then
  #RECO MACRO
  echo "==> root -l -b -q  $par2($par3,$par4,$par5,$par6)"
  time  root -l -b -q ''$par2'('$par3',"'$par4'","'$par5'",'$par6')' 

  elif [[ "$macrotype" == "recotime" ]]
  then
  #RECO MACRO
  echo "==> root -l -b -q  $par2($par3,$par4,$par5,$par6)"
  time  root -l -b -q ''$par2'('$par3',"'$par4'","'$par5'","'$par6'")' 

  elif [[ "$macrotype" == "thermal" ]]
  then
  #KF thermal source
  echo "==> root -l -b -q  $par2($par3,$par4,$par5)"
  time  root -l -b -q ''$par2'('$par3','$par4',"'$par5'")' 
  
  elif [[ "$macrotype" == "hadronana" ]]
  then
  #HADRON ANALYSIS FROM Norbert
  echo "==> root -l -b -q  $par2($par3,$par4,$par5,$par6,$par7)"
  time  root -l -b -q ''$par2'('$par3',"'$par4'","'$par5'","'$par6'",'$par7')'

  elif [[ "$macrotype" == "anatree" ]]
  then
  #TREE ANALYSIS  MAKER 
  echo "==> root -l -b -q  $par2($par3,$par4,$par5)"
  time  root -l -b -q ''$par2'("'$par3'","'$par4'","'$par5'")'

  fi

  status=$?

  echo "------------------------------------"
  echo "OUTPUT:"

  if [ $status -ne 0 ]
    then
    echo "JOB: $SLURM_JOBID CRASHED ON HOST: $host"
  fi

cd ${workDir}
rm -rf ${tempDir}

#   END EDIT YOUR EXECUT JOB!
###################################################################
###################################################################
 
  echo ""               
  echo "--------------------------------"
  echo "Job with params "
  echo "par1 = ${par1}"  
  echo "par2 = ${par2}"  
  echo "par3 = ${par3}"  
  echo "par4 = ${par4}"  
  echo "par5 = ${par5}"  
  echo "par6 = ${par6}"  
  echo "par7 = ${par7}" 
  echo "par8 = ${par8}"  
  echo "finsished!"      
  echo "--------------------------------"
  echo ""
    
    
  echo ""               
  echo "--------------------------------"
  echo "MONITOR ENVIRONMENT:"
  echo "SLURM_JOBID        : " $SLURM_JOBID
  echo "SLURM_ARRAY_JOB_ID : " $SLURM_ARRAY_JOB_ID
  echo "SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID
  echo "RUNNING ON HOST    : " $(hostname)
  echo "WORKING DIR        : " $(pwd)
  echo "USER is            : " $USER
  echo "DISK USAGE /tmp    :------------"
  df -h /tmp 
  echo "--------------------------------"
  
     
  date $format
 
  sleep 2

  mv ${pathoutputlog}/slurm-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.out ${pathoutputlog}/slurm-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.log
