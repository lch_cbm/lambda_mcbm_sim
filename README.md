# HowTo use this repository:
- make sure you have access to CBM-ROOT installation and personal folder on /lustre/cbm/users
- adjust the path to dirs in sendJobArrayScript.sh according to your specifics, use the same path for logs also in runUrqmdChain.sh
- adjust the path in runUrqmdChain.sh and the individual steps for your analysis purposes - but make sure that the mcbm_MACROS.C are ready for the params as in sendJobArrayScript.sh
- create folders:
```
mkdir JAfiles
mkdir nohup_outputs
```

## Proper use of runUrqmdChain is as follows:
```
nohup bash runUrqmdChain.sh auau mcbm_beam_2022_04 geant3 < /dev/null > nohup_outputs/auau_mcbm202204_geant3.prod 2>&1 &
```
#### Break-down of the line:
`nohup` make sure that script will run on background even if you logout\
`bash` what program should execute the script\
`runUrqmdChain.sh` name of the script to be executed with parameters:

- first is the collision system (mandatory)
- second is the setup geometry (mandatory)
- others are optional -> geant3 is probably good idea to use since geant4 takes more time and produce much larged outputs

`< /dev/null` dummy nohup standard input (in order not to have "nohup: ignoring input" message in stderr)\
`> filename` name of file where standard output messages will be stored (in the case of runUrqmdChain.sh it also include important machine and process ID if one wants to kill it)\
`2>&1` redirect standard error output to the same as stdout\
`&` run on background, so one can still use the terminal window for other things

#### runUrqmdChain script options
`-d` set the debugging flag on which will result in very large nohup ouput (one should consider setting maximum number of events or time)\
`-e NUMBER` set NUMBER of events to be processed (default value is 100 000, good for debugging)\
`-j NUMBER` set NUMBER of jobs to be sent within jobArray (default is 1000, good for debugging)\
`-k` no log files will be deleted (normal behaviour is to delete log files for COMPLETED jobs) = keep logs\
`-t SLURM_TIME_FORMAT` set the maximum time for all submitted jobs (default value varies depending on simulation step, format is D-HH:MM:SS, good for debugging)\
`-v` set the verbose mode globally on (probably not really recommanded to use unless deep debugging the code)
 
#### Suggestions
- one can check the stdout-file from nohup to monitor the individual steps of analysis
- when all is finished -> one can check the /path2logs/slurm-*.checkme files where the logs from not completed tasks is stored
- for testing consider to set "debugging" variable to 1 in runUrqmdChain.sh
